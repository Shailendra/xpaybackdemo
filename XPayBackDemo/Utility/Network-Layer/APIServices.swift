//
//  APIServices.swift
//  XPayBackDemo
//
//  Created by Shailendra Kumar Gupta on 01/02/24.
//
import Foundation
import SwiftUI


class APIServices : NSObject{
    
    func loadData(url: String,method : HTTPMethod = .GET,param : [String: Any]?, success:@escaping (Data) -> (),failure:@escaping (String) -> ()) {
        
        print("param====\(String(describing: param))")
        print("url====\(url)")
        let session = URLSession(configuration: .default)
        var request = URLRequest(url: URL(string:url)!)
        request.httpMethod = method.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        if param != nil {
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param as Any, options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
                return
            }
        }
        let task = session.dataTask(with: request) { data, response, error in
            if let error = error {
                print("Post Request Error: \(error.localizedDescription)")
                return
            }
            let httpResponse = response as? HTTPURLResponse
            if (200...299).contains(httpResponse!.statusCode) {
                
            }else if httpResponse!.statusCode == 401{
                failure("\(httpResponse!.statusCode)")
                return
            }
            guard let responseData = data else {
                print("nil Data received from the server")
                return
            }
            
            do {
                if let jsonResponse = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) as? [String: Any] {
                    print("jsonResponse===\(jsonResponse)")
                    success(responseData)
                } else {
                    print("data maybe corrupted or in wrong format")
                    throw URLError(.badServerResponse)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
        task.resume()
    }
}
