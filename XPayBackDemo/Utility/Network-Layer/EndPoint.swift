//
//  EndPoint.swift
//  XPayBackDemo
//
//  Created by Shailendra Kumar Gupta on 01/02/24.
//

import Foundation

struct EndPointURL {
    
    static let getDropdownsDetails = "get-dropdowns-details"
}
