//
//  LoaderView.swift
//  XPayBackDemo
//
//  Created by Shailendra Kumar Gupta on 01/02/24.
//

import SwiftUI

struct LoaderView: View {
    
    var body: some View {
        ZStack{
            Color.gray.opacity(0.2)
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle(tint: Color(ColorName.themeColor)))
                .scaleEffect(2)
                
        }.edgesIgnoringSafeArea(.all)
    }
}

#Preview {
    LoaderView()
}

