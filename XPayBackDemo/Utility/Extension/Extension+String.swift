//
//  Extension+Swift.swift
//  XPayBackDemo
//
//  Created by Shailendra Kumar Gupta on 01/02/24.
//

import Foundation
import SwiftUI

extension String {
    
    static let kEmpty         = ""
    static let kOneSpace      = " "
    static let kOnDash        = "-"
}

