//
//  Structure.swift
//  XPayBackDemo
//
//  Created by Shailendra Kumar Gupta on 01/02/24.
//

import Foundation
import UIKit

struct ImageName {
    
    static let kSplash  = "ic-splash"
    static let kBACK    = "ic-back"
    static let kProfile = "placeholderImage"
}

struct ColorName {
    
    static let bgColor      = "bgColor"
    static let themeColor   = "themeColor"
}

struct FontName {
    
    static let kPoppinsBold      = "Poppins-Bold"
    static let kPoppinsRegular   = "Poppins-Regular"
    static let kPoppinsMedium    = "Poppins-Medium"
    static let kPoppinsSemiBold  = "Poppins-SemiBold"
    static let kPoppinsLight     = "Poppins-Light"
}
