//
//  Message.swift
//  XPayBackDemo
//
//  Created by Shailendra Kumar Gupta on 01/02/24.
//
import Foundation
import SwiftUI

struct Message {
    
    static let InternetConnection  = "Please Check Your Internet Connection"
    static let SomeThingWentWrong  = "Something went wrong please try again!!"
}
