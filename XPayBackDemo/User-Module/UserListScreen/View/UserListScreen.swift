//
//  UserListScreen.swift
//  XPayBackDemo
//
//  Created by Shailendra Kumar Gupta on 01/02/24.
//

import SwiftUI

struct UserListScreen: View {
    
    //MARK: - PROPERTIES
    @Environment(\.presentationMode) var presentationMode
    @StateObject var vm              = UserListVM()
    @State var isUserCardClicked     = false
    @State var userId                : Int = 0
    
    var body: some View {
        NavigationView{
            
            ZStack{
                VStack(alignment: .center){
                    Text("Users List")
                        .multilineTextAlignment(.leading)
                        .font(.custom(FontName.kPoppinsBold, size: 20))
                    ScrollView(showsIndicators: false){
                        LazyVStack {
                            let users  = self.vm.userModel
                            ForEach(users.indices, id: \.self ) { i in
                                NavigationLink(destination: UserDetailsScreen(userId: self.userId).navigationBarHidden(true), isActive: $isUserCardClicked){
                                    UserListCard(user: users[i])
                                        .onTapGesture {
                                            self.userId = users[i]?.id ?? 0
                                            self.isUserCardClicked = true
                                        }.onAppear {
                                            self.vm.loadMoreContent(currentItem: users[i])
                                        }
                                }
                            }
                        }
                    }.padding([.leading,.trailing],5)
                }
                if self.vm.isLoaderShow {
                    LoaderView()
                }
            }.alert(isPresented:$vm.showAlertToView) {
                Alert(
                    title: Text(""),
                    message: Text(self.vm.message),
                    primaryButton: .default(Text("")){},
                    secondaryButton:.destructive(Text("OK")){}
                )
            }
        }
    }
}

#Preview {
    UserListScreen()
}
