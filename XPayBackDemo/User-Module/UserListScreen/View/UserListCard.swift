//
//  UserListCard.swift
//  XPayBackDemo
//
//  Created by Shailendra Kumar Gupta on 01/02/24.
//

import SwiftUI

struct UserListCard: View {
    
    var user : Users?
    
    var body: some View {
        VStack {
            VStack(alignment:.leading) {
                HStack {
                    if self.user?.phone ?? .kEmpty == .kEmpty {
                        Image(ImageName.kProfile)
                            .resizable()
                            .frame(width: 100,height: 100)
                            .cornerRadius(50)
                        
                    }else{
                        AsyncImage(url: URL(string: self.user?.image ?? .kEmpty)) { image in
                            image.resizable()
                                .scaledToFill()
                        } placeholder: {
                            Color.purple.opacity(0.1)
                        } .frame(width: 100, height: 100)
                            .cornerRadius(50)
                    }
                    Spacer()
                    VStack(alignment:.leading){
                        Group {
                            HStack {
                                Text("User ID :").font(.custom(FontName.kPoppinsSemiBold, size: 14))
                                Text("\(self.user?.id ?? 0)").font(.custom(FontName.kPoppinsRegular, size: 14))
                            }
                            HStack {
                                Text("User Name :").font(.custom(FontName.kPoppinsSemiBold, size: 14))
                                Text(self.user?.username ?? .kEmpty).font(.custom(FontName.kPoppinsRegular, size: 14))
                            }
                            HStack {
                                Text("Full Name :").font(.custom(FontName.kPoppinsSemiBold, size: 14))
                                Text("\(self.user?.firstName ?? .kEmpty)" + .kOneSpace + "\(self.user?.maidenName ?? .kEmpty)" + .kOneSpace + "\(self.user?.lastName ?? .kEmpty)").font(.custom(FontName.kPoppinsRegular, size: 14))
                            }
                            HStack {
                                Text("Email :").font(.custom(FontName.kPoppinsSemiBold, size: 14))
                                Text(self.user?.email ?? .kEmpty).font(.custom(FontName.kPoppinsRegular, size: 14))
                            }
                            HStack {
                                Text("Phone :").font(.custom(FontName.kPoppinsSemiBold, size: 14))
                                Text(self.user?.phone ?? .kEmpty).font(.custom(FontName.kPoppinsRegular, size: 14))
                            }
                            HStack {
                                Text("Age :").font(.custom(FontName.kPoppinsSemiBold, size: 14))
                                Text("\(self.user?.age ?? 0)").font(.custom(FontName.kPoppinsRegular, size: 14))
                            }
                        }.foregroundColor(Color.black)
                    }.padding(.top,15)
                        .padding(.bottom,15)
                        .frame(width: UIScreen.main.bounds.size.width-120)
                }
            }
        }.background(Color(ColorName.bgColor))
        .cornerRadius(6)
    }
}

#Preview {
    UserListCard()
}
