//
//  UserListVM.swift
//  XPayBackDemo
//
//  Created by Shailendra Kumar Gupta on 01/02/24.
//

import Foundation

class UserListVM : ObservableObject {
    
    //MARK: - PROPERTIES
    @Published var message         : String = .kEmpty
    @Published var showAlertToView : Bool   = false
    @Published var isLoaderShow    : Bool   = false
    var API                        : APIServices!
    var model                      : UserCodable?
    var skip                       : Int = 0
    var total                      = 0
    var userModel                  : [Users?] = []
    
    init(){
        self.API = APIServices()
        self.CallWebAPI()
    }
    
    func CallWebAPI(){
        self.isLoaderShow = true
        if !Reachability.isConnectedToNetwork(){
            self.commonMessge(msg: Message.InternetConnection)
            return
        }
        self.API.loadData(url: BaseURL.kDevURL + JSON_KEY.klimitAndSkip + "\(self.skip)" ,method: .GET, param: nil) { data in
            do {
                let result = try JSONDecoder().decode(UserCodable.self, from: data)
                DispatchQueue.main.async {
                    self.model = result
                    self.total = result.total ?? 0
                    for i in 0..<(self.model?.users?.count ?? 00) {
                        self.userModel.append(self.model?.users?[i])
                    }
                    self.isLoaderShow = false
                }
            } catch {
                self.commonMessge(msg: Message.SomeThingWentWrong)
            }
        }  failure: { error in
            self.commonMessge(msg: Message.SomeThingWentWrong)
        }
    }
    
    func loadMoreContent(currentItem item: Users?){
        if self.skip != self.total {
            self.skip = self.skip + 10
            self.CallWebAPI()
        }
    }
    
    func commonMessge(msg:String){
        DispatchQueue.main.async {
            self.isLoaderShow    = true
            self.showAlertToView = true
            self.message         = msg
        }
    }
}
