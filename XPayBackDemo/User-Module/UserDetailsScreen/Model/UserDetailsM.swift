//
//  UserDetailsM.swift
//  XPayBackDemo
//
//  Created by Shailendra Kumar Gupta on 01/02/24.
//

import Foundation

struct UserDetailsCodable : Codable {
    
    let id         : Int?
    let firstName  : String?
    let lastName   : String?
    let maidenName : String?
    let age        : Int?
    let gender     : String?
    let email      : String?
    let phone      : String?
    let username   : String?
    let password   : String?
    let birthDate  : String?
    let image      : String?
    let bloodGroup : String?
    let height     : Int?
    let weight     : Double?
    let eyeColor   : String?
    let hair       : Hair?
    let domain     : String?
    let ip         : String?
    let address    : Address?
    let macAddress : String?
    let university : String?
    let bank       : Bank?
    let company    : Company?
    let ein        : String?
    let ssn        : String?
    let userAgent  : String?
    let crypto     : Crypto?
    
    enum CodingKeys: String, CodingKey {
        
        case id         = "id"
        case firstName  = "firstName"
        case lastName   = "lastName"
        case maidenName = "maidenName"
        case age        = "age"
        case gender     = "gender"
        case email      = "email"
        case phone      = "phone"
        case username   = "username"
        case password   = "password"
        case birthDate  = "birthDate"
        case image      = "image"
        case bloodGroup = "bloodGroup"
        case height     = "height"
        case weight     = "weight"
        case eyeColor   = "eyeColor"
        case hair       = "hair"
        case domain     = "domain"
        case ip         = "ip"
        case address    = "address"
        case macAddress = "macAddress"
        case university = "university"
        case bank       = "bank"
        case company    = "company"
        case ein        = "ein"
        case ssn        = "ssn"
        case userAgent  = "userAgent"
        case crypto     = "crypto"
    }
    
    init(from decoder: Decoder) throws {
        
        let values      = try decoder.container(keyedBy: CodingKeys.self)
        self.id         = try values.decodeIfPresent(Int.self, forKey: .id)
        self.firstName  = try values.decodeIfPresent(String.self, forKey: .firstName)
        self.lastName   = try values.decodeIfPresent(String.self, forKey: .lastName)
        self.maidenName = try values.decodeIfPresent(String.self, forKey: .maidenName)
        self.age        = try values.decodeIfPresent(Int.self, forKey: .age)
        self.gender     = try values.decodeIfPresent(String.self, forKey: .gender)
        self.email      = try values.decodeIfPresent(String.self, forKey: .email)
        self.phone      = try values.decodeIfPresent(String.self, forKey: .phone)
        self.username   = try values.decodeIfPresent(String.self, forKey: .username)
        self.password   = try values.decodeIfPresent(String.self, forKey: .password)
        self.birthDate  = try values.decodeIfPresent(String.self, forKey: .birthDate)
        self.image      = try values.decodeIfPresent(String.self, forKey: .image)
        self.bloodGroup = try values.decodeIfPresent(String.self, forKey: .bloodGroup)
        self.height     = try values.decodeIfPresent(Int.self, forKey: .height)
        self.weight     = try values.decodeIfPresent(Double.self, forKey: .weight)
        self.eyeColor   = try values.decodeIfPresent(String.self, forKey: .eyeColor)
        self.hair       = try values.decodeIfPresent(Hair.self, forKey: .hair)
        self.domain     = try values.decodeIfPresent(String.self, forKey: .domain)
        self.ip         = try values.decodeIfPresent(String.self, forKey: .ip)
        self.address    = try values.decodeIfPresent(Address.self, forKey: .address)
        self.macAddress = try values.decodeIfPresent(String.self, forKey: .macAddress)
        self.university = try values.decodeIfPresent(String.self, forKey: .university)
        self.bank       = try values.decodeIfPresent(Bank.self, forKey: .bank)
        self.company    = try values.decodeIfPresent(Company.self, forKey: .company)
        self.ein        = try values.decodeIfPresent(String.self, forKey: .ein)
        self.ssn        = try values.decodeIfPresent(String.self, forKey: .ssn)
        self.userAgent  = try values.decodeIfPresent(String.self, forKey: .userAgent)
        self.crypto     = try values.decodeIfPresent(Crypto.self, forKey: .crypto)
    }
}
