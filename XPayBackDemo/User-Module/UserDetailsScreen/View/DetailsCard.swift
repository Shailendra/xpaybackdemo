//
//  DetailsCard.swift
//  XPayBackDemo
//
//  Created by Shailendra Kumar Gupta on 01/02/24.
//

import SwiftUI

struct DetailsCard : View {
    
    //MARK: - PROPERTIES
    var key            : String
    var value          : String
    
    var body: some View{
        ZStack{
            HStack{
                Group {
                    Text(self.key)
                        .font(.custom(FontName.kPoppinsBold, size: 14))
                    Spacer()
                    Text(self.value)
                        .font(.custom(FontName.kPoppinsRegular, size: 14))
                } .foregroundColor(Color.black)
                    .multilineTextAlignment(.leading)
                    .padding([.top,.bottom],5)
            }.padding([.leading,.trailing],5)
        }
        .background(Color(ColorName.bgColor))
            .cornerRadius(5)
    }
}
