//
//  UserDetailsScreen.swift
//  XPayBackDemo
//
//  Created by Shailendra Kumar Gupta on 01/02/24.
//

import SwiftUI

struct UserDetailsScreen: View {
    
    //MARK: - PROPERTIES
    @Environment(\.presentationMode) var presentationMode
    @StateObject var vm              = UserDetailsVM()
    var userId                       : Int = 0
    
    var body: some View {
        
        ZStack{
            VStack(alignment: .leading){
                HStack {
                    Button(action: {
                        self.presentationMode.wrappedValue.dismiss()
                    },
                           label: {
                        Image(ImageName.kBACK)
                            .resizable()
                            .padding([.top,.bottom,.trailing])
                            .frame(width: 35,height: 45)
                    }).padding([.leading])
                    Text("User Details")
                        .multilineTextAlignment(.leading)
                        .font(.custom(FontName.kPoppinsBold, size: 20))
                }
                if self.vm.model != nil {
                    ScrollView(showsIndicators: false){
                        UserListCard(user: self.vm.model)
                        DetailsCard(key: "Gender", value: self.vm.model?.gender ?? .kEmpty)
                        DetailsCard(key: "Password", value: self.vm.model?.password ?? .kEmpty)
                        DetailsCard(key: "Blood Group", value: self.vm.model?.bloodGroup ?? .kEmpty)
                        DetailsCard(key: "Height", value: "\(self.vm.model?.height ?? 0)")
                        DetailsCard(key: "Weight", value: "\(self.vm.model?.weight ?? 0)")
                        DetailsCard(key: "Type", value: self.vm.model?.hair?.type ?? .kEmpty)
                        DetailsCard(key: "Domain", value: self.vm.model?.domain ?? .kEmpty)
                        DetailsCard(key: "IP", value: self.vm.model?.ip ?? .kEmpty)
                        DetailsCard(key: "Mac Address", value: self.vm.model?.macAddress ?? .kEmpty)
                        DetailsCard(key: "University", value: self.vm.model?.university ?? .kEmpty)
                        DetailsCard(key: "EIN", value: self.vm.model?.ein ?? .kEmpty)
                        DetailsCard(key: "SSN", value: self.vm.model?.ssn ?? .kEmpty)
                        DetailsCard(key: "User Agent", value: self.vm.model?.userAgent ?? .kEmpty)
                        DetailsCard(key: "Address", value: "\(self.vm.model?.address?.address ?? .kEmpty)" + .kOneSpace + "\(self.vm.model?.address?.city ?? .kEmpty)" + .kOneSpace + "\(self.vm.model?.address?.state ?? .kEmpty)" + .kOneSpace + "\(self.vm.model?.address?.postalCode ?? .kEmpty)")
                        DetailsCard(key: "Address Coordinates", value: "\(self.vm.model?.address?.coordinates?.lat ?? 0)" + "\(self.vm.model?.address?.coordinates?.lng ?? 0)")
                        DetailsCard(key: "Card Number", value: self.vm.model?.bank?.cardNumber ?? .kEmpty)
                        DetailsCard(key: "Card Type", value: self.vm.model?.bank?.cardType ?? .kEmpty)
                        DetailsCard(key: "Card Expire", value: self.vm.model?.bank?.cardExpire ?? .kEmpty)
                        DetailsCard(key: "Currency", value: self.vm.model?.bank?.currency ?? .kEmpty)
                        DetailsCard(key: "BAN", value: self.vm.model?.bank?.iban ?? .kEmpty)
                        DetailsCard(key: "Title", value: self.vm.model?.company?.title ?? .kEmpty)
                        DetailsCard(key: "Department", value: self.vm.model?.company?.department ?? .kEmpty)
                        DetailsCard(key: "Company Address", value: "\(self.vm.model?.company?.address?.address ?? .kEmpty)" + .kOneSpace + "\(self.vm.model?.company?.address?.city ?? .kEmpty)" + .kOneSpace + "\(self.vm.model?.company?.address?.state ?? .kEmpty)" + .kOneSpace + "\(self.vm.model?.company?.address?.postalCode ?? .kEmpty)")
                        DetailsCard(key: "Company Coordinates", value: "\(self.vm.model?.company?.address?.coordinates?.lat ?? 0)" + "\(self.vm.model?.company?.address?.coordinates?.lng ?? 0)")
                        DetailsCard(key: "Coin:", value: self.vm.model?.crypto?.coin ?? .kEmpty)
                        DetailsCard(key: "Wallet", value: self.vm.model?.crypto?.wallet ?? .kEmpty)
                        DetailsCard(key: "Network", value: self.vm.model?.crypto?.network ?? .kEmpty)
                    }.padding([.leading,.trailing],10)
                        .frame(width: UIScreen.main.bounds.size.width-10)
                }else{
                    ScrollView(showsIndicators: false) {
                        Text("No Data Available")
                            .font(.headline)
                            .foregroundColor(.black)
                            .padding(.top,UIScreen.main.bounds.size.height/3)
                    }
                }
            }
            if self.vm.isLoaderShow {
                LoaderView()
            }
        }
        .alert(isPresented:$vm.showAlertToView) {
            Alert(title: Text(""), message: Text(self.vm.message),primaryButton: .default(Text("")) {},
                  secondaryButton:.destructive(Text("OK")) {}
            )
        }
        .onAppear{
            self.vm.CallWebAPI(userID: self.userId)
        }
    }
}

#Preview {
    UserDetailsScreen()
}
