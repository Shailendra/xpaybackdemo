//
//  UserDetailsVM.swift
//  XPayBackDemo
//
//  Created by Shailendra Kumar Gupta on 01/02/24.
//

import Foundation

class UserDetailsVM : ObservableObject {
    
    //MARK: - PROPERTIES
    @Published var message            : String = .kEmpty
    @Published var showAlertToView    : Bool   = false
    @Published var isLoaderShow       : Bool   = false
    var API                           : APIServices!
    var model                         : Users?
  
    init(){
        self.API = APIServices()
    }
    
    func CallWebAPI(userID: Int){
        self.isLoaderShow = true
        if !Reachability.isConnectedToNetwork(){
            self.commonMessge(msg: Message.InternetConnection)
            return
        }
        self.API.loadData(url: BaseURL.kDevURL + "/\(userID)" ,method: .GET, param: nil) { data in
            do {
                let result = try JSONDecoder().decode(Users.self, from: data)
                DispatchQueue.main.async {
                    self.model = result
                    self.isLoaderShow = false
                }
            } catch {
                self.commonMessge(msg: Message.SomeThingWentWrong)
            }
        }  failure: { error in
            self.commonMessge(msg: Message.SomeThingWentWrong)
        }
    }
    
    func commonMessge(msg:String){
        DispatchQueue.main.async {
            self.isLoaderShow    = true
            self.showAlertToView = true
            self.message         = msg
        }
    }
}
