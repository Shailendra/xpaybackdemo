//
//  SplashScreen.swift
//  XPayBackDemo
//
//  Created by Shailendra Kumar Gupta on 01/02/24.
//

import SwiftUI

struct SplashScreen: View {
    
    //MARK: - PROPERTIES
    @State var isActive             : Bool = false
    @State var finalAlertAcceptance : Bool = true
   
    var body: some View {
        ZStack {
            if self.isActive {
                UserListScreen()
            } else {
                Image(ImageName.kSplash)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 300, height: 300)
            }
        }.onAppear{
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.isActive = true
            }
        }
    }
}

#Preview {
    SplashScreen()
}
